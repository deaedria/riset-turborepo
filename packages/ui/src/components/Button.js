import React, { useEffect } from 'react';

const Button = (props) => {
  useEffect(() => {
    console.log('Button Component');
  }, []);

  return (
    <button
      style={{
        backgroundColor: 'lavenderblush',
        padding: '10px 10px',
        fontSize: '20px',
      }}
    >
      Hello, {props.children}!
    </button>
  );
};

export default Button;
