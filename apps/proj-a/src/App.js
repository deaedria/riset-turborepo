import React from "react";
import { ThemeProvider } from "@material-ui/core/styles";
import "./App.css";
import { Button, Text, ThemeStyles } from "ui";
import HomePage from "./HomePage";

function App() {
  return (
    <div className="App">
      <ThemeProvider theme={ThemeStyles}>
        <header className="App-header">
          <Button>Project A </Button>
          <Text />
          <HomePage />
        </header>
      </ThemeProvider>
    </div>
  );
}

export default App;
